package com.girindhra.mystopwatch.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.girindhra.mystopwatch.room.model.Employee

@Dao
interface EmpDao {

 @Insert
 fun insertEmp(employee: Employee)

 @Query("select * from employee")
 fun getEmp():List<Employee>


}