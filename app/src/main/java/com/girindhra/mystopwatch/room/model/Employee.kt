package com.girindhra.mystopwatch.room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Employee(@PrimaryKey val id:Int, @ColumnInfo(name="name") val name:String, @ColumnInfo(name="dob")val dob:String, @ColumnInfo(name="address")val address:String)
