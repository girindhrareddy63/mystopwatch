package com.girindhra.mystopwatch.room


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.girindhra.mystopwatch.room.dao.EmpDao
import com.girindhra.mystopwatch.room.model.Employee

@Database(entities = [Employee::class],version = 1)
abstract class DataBase: RoomDatabase() {
    abstract fun empDao():EmpDao

    companion object {
        val dataBaseName ="empDb.db"
        @Volatile
        private var instance: DataBase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }
        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            DataBase::class.java, dataBaseName
        )
            .build()
    }
}